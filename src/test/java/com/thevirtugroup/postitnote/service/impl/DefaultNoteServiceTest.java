package com.thevirtugroup.postitnote.service.impl;

import com.thevirtugroup.postitnote.exception.UnauthorizedException;
import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;
import com.thevirtugroup.postitnote.security.SecurityUserWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class DefaultNoteServiceTest {

    private static final long CORRECT_USER = 10L;
    private static final long WRONG_USER = 11L;

    @Mock
    private NoteRepository noteRepository;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private Authentication authentication;

    @Mock
    private SecurityUserWrapper userWrapper;

    @Mock
    private List<Note> noteList;

    @Mock
    private Note note;

    private DefaultNoteService defaultNoteService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        defaultNoteService = new DefaultNoteService(noteRepository);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(userWrapper);
        when(userWrapper.getId()).thenReturn(CORRECT_USER);
        when(note.getId()).thenReturn(1L);
        when(note.getAuthorId()).thenReturn(CORRECT_USER);
        when(noteRepository.getNote(1L)).thenReturn(note);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void getCallsRepositoryWithFilter() {
        defaultNoteService.getNotes();
        verify(noteRepository).getNotes(userWrapper.getId());
    }

    @Test
    public void getReturnsUnmodifiedResult() {
        when(noteRepository.getNotes(userWrapper.getId())).thenReturn(noteList);
        List<Note> returnedNotes = defaultNoteService.getNotes();
        verifyNoInteractions(noteList);
        assertEquals(noteList, returnedNotes);
    }

    @Test(expected = UnauthorizedException.class)
    public void updateFailsWhenWrongUser() {
        when(userWrapper.getId()).thenReturn(WRONG_USER);
        defaultNoteService.updateNote(note);
    }

    @Test
    public void updateCallsRepoWhenCorrectUser() {
        defaultNoteService.updateNote(note);
        verify(noteRepository).updateNote(note);
    }

    @Test(expected = UnauthorizedException.class)
    public void deleteFailsWhenWrongUser() {
        when(userWrapper.getId()).thenReturn(WRONG_USER);
        defaultNoteService.deleteNote(note.getId());
    }

    @Test
    public void deleteCallsRepoWhenCorrectUser() {
        defaultNoteService.deleteNote(note.getId());
        verify(noteRepository).deleteNote(note.getId());
    }

    @Test
    public void setsUserWhenCreating() {
        defaultNoteService.createNote(note);
        verify(noteRepository).createNote(note);
    }
}