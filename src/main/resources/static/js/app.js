(function(){

    var app = angular.module('notesApp', ['ngRoute', 'ngMaterial', 'ngMessages']);

    app.config(['$locationProvider', '$routeProvider',
      function ($locationProvider, $routeProvider) {

        $routeProvider
          .when('/', {
            templateUrl: '/partials/notes-view.html',
            controller: 'notesController'
          })
          .when('/login', {
             templateUrl: '/partials/login.html',
             controller: 'loginController',
          })
          .otherwise('/');
      }
    ]);

    app.config(function($httpProvider) {
      $httpProvider.interceptors.push(['$rootScope', '$q', function($rootScope, $q) {
        return {
          responseError: function(res) {
            if(res.status === 401) {
                $rootScope.$broadcast("doLogout");
            }
            return $q.reject(res);
          }
        };
      }]);
    });

    app.run(['$rootScope', '$location', 'AuthService', function ($rootScope, $location, AuthService) {
      $rootScope.$on('$routeChangeStart', function (event) {

          if ($location.path() == "/login"){
             return;
          }

          if (!AuthService.isLoggedIn()) {
              console.log('DENY');
              event.preventDefault();
              $location.path('/login');
          }
      });
    }]);


    app.service('NotesService', function($http) {

        function getNotes() {

            return $http.get("api/notes")
                .then(function(success) {
                    console.log(success);
                    return success.data;
                })
                .catch(function(error){
                  console.log(error);
                });
        }

        function addNote(note) {

            return $http.post("api/notes", note).then(function(success) {
                note.id = success.data;
                return note;
            })
            .catch(function(error){
               console.log(error);
               return -1;
            })
        }

        function updateNote(note) {

            return $http.put("api/notes/", note).then(function(success) {
                return note;
            })
            .catch(function(error){
               console.log(error);
               return -1;
            });

        }

        function deleteNote(id) {


            return $http.delete("api/notes/" + id).then(function(success) {
                return id;
            })
            .catch(function(error){
                console.log(error);
                return -1;
            });
        }


        return {
            getNotes: getNotes,
            addNote: addNote,
            updateNote: updateNote,
            deleteNote: deleteNote

        };
    });

    app.service('AuthService', function($http, $location){
        var loggedUser = null;

        function login (username, password){
            return $http.post("api/login", {username: username, password: password}).then(function(user){
                loggedUser = user;
                return true;
            })
            .catch(function(error){
                loggedUser = null;
                return false;
            })
        }

        function isLoggedIn(){
            return loggedUser != null;
        }

        function logout(){
            loggedUser = null;
            $location.path('login')
        }

        return {
            login : login,
            isLoggedIn: isLoggedIn,
            logout: logout
        }
    });

    app.controller('logoutController', ['$scope', 'AuthService', function($scope, AuthService) {

    $scope.logout = function() {
        AuthService.logout();
    };
    }])

    app.controller('loginController', function($scope, AuthService, $location){

    $scope.invalidCreds = false;
    $scope.login = {
        username : null,
        password : null
    };

    $scope.login = function(){
        AuthService
            .login($scope.login.username, $scope.login.password)
            .then(function(isLoggedIn){

                if (isLoggedIn) {
                   $location.path('/');
                 }

            $scope.invalidCreds = !isLoggedIn;
        });
    };
    });


    app.controller('notesController', function($scope, NotesService, AuthService){

    $scope.notes = [];
    $scope.note = null;

    $scope.$on('$viewContentLoaded', function() {

        NotesService.getNotes().then(function(notes) {
            $scope.notes = notes;
        });
    });

    $scope.$on("doLogout", function() {
        AuthService.logout();
    });

    $scope.isEditCreateView = false;

    $scope.newNoteView = function(){

        $scope.note = {};
    };

    $scope.cancel = function() {

        $scope.note = null;
    }

    $scope.deleteNote = function (id) {
      var r = confirm("Are you sure you want to delete this note?");
      if (r == true){
        NotesService.deleteNote(id).then(function(id) {
            $scope.notes = $scope.notes.filter(i => i.id !== id)
        })
      }
    };

    $scope.selectNote = function(noteId) {
        $scope.note = $scope.notes.find(i => i.id === noteId);
    };

    $scope.saveNote = function() {

        if($scope.note !== null && $scope.note.id !== undefined) {
            updateNote();
            return;
        }

        createNote();
    }

    function updateNote() {
            NotesService.updateNote($scope.note)
                .then(function(note){

                })
                .catch(function(error) {
                    console.log(error);
                });
    }


    function createNote() {
        NotesService.addNote($scope.note)
            .then(function(note){
                $scope.notes.push(note);
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    });

})();