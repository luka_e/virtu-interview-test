package com.thevirtugroup.postitnote.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Note {

    private Long id;
    private String title;
    private String content;
    private Long createdAt;
    private Long updatedAt;
    private Long authorId;
}
