package com.thevirtugroup.postitnote.exception;

public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException() {
        super("This user is not authorised for the requested action.");
    }
}
