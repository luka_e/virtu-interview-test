package com.thevirtugroup.postitnote.exception;

public class NotAuthenticatedException extends RuntimeException{

    public NotAuthenticatedException(final String message) {
        super(message);
    }
}
