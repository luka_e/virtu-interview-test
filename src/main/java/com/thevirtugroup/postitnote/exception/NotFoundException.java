package com.thevirtugroup.postitnote.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException() {
        super("The requested resource cannot be found.");
    }
}
