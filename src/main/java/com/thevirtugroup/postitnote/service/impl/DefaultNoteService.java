package com.thevirtugroup.postitnote.service.impl;

import com.thevirtugroup.postitnote.exception.UnauthorizedException;
import com.thevirtugroup.postitnote.model.Note;
import com.thevirtugroup.postitnote.repository.NoteRepository;
import com.thevirtugroup.postitnote.security.SecurityContext;
import com.thevirtugroup.postitnote.service.NoteService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DefaultNoteService implements NoteService {

    private final NoteRepository noteRepository;

    public DefaultNoteService(final NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Override
    public List<Note> getNotes() {
        return noteRepository.getNotes(SecurityContext.getLoggedInUser().getId());
    }

    @Override
    public void updateNote(Note note) {
        if(!SecurityContext.getLoggedInUser().getId().equals(note.getAuthorId())) {
            throw new UnauthorizedException();
        }
        noteRepository.updateNote(note);
    }

    @Override
    public void deleteNote(Long id) {
        if(!SecurityContext.getLoggedInUser().getId().equals(noteRepository.getNote(id).getAuthorId())) {
            throw new UnauthorizedException();
        }
        noteRepository.deleteNote(id);
    }

    @Override
    public Long createNote(Note note) {
        note.setAuthorId(SecurityContext.getLoggedInUser().getId());
        return noteRepository.createNote(note);
    }
}
