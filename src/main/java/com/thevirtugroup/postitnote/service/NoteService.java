package com.thevirtugroup.postitnote.service;

import com.thevirtugroup.postitnote.model.Note;

import java.util.List;

public interface NoteService {

    List<Note> getNotes();

    void updateNote(Note note);

    void deleteNote(Long id);

    Long createNote(Note note);
}
