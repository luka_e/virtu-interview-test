package com.thevirtugroup.postitnote.util;

public class StringUtils {

    private StringUtils() {
    }

    public static boolean isNullOrEmpty(String... strings) {
        for (String string : strings) {
            if(string == null || string.isEmpty()) {
                return true;
            }
        }
        return false;
    }
}
