package com.thevirtugroup.postitnote.repository;

import com.thevirtugroup.postitnote.exception.NotFoundException;
import com.thevirtugroup.postitnote.model.Note;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class NoteRepository {

    private final List<Note> notes;

    public NoteRepository() {
        notes = new ArrayList<>();
        for(long i = 1; i < 10; i++) {
            notes.add(
                    new Note(
                            i,
                            "test " + i,
                            "note text " + i,
                            System.currentTimeMillis(),
                            System.currentTimeMillis(),
                            i % 2 == 0 ? 999L : 1L));
        }
    }

    public Note getNote(long id) {
        return notes.stream().filter(note -> note.getId().equals(id)).findFirst().orElseThrow(NotFoundException::new);
    }

    public List<Note> getNotes(final long userId) {
        return notes.stream().filter(note -> note.getAuthorId()==userId).collect(Collectors.toList());
    }

    public void deleteNote(final Long id) {
        notes.removeIf(note -> note.getId().equals(id));
    }

    public Long createNote(final Note note) {
        final Long maxId = notes.stream().map(Note::getId).max(Long::compareTo).orElse(0L);
        note.setId(maxId + 1);
        notes.add(note);
        return note.getId();
    }

    public void updateNote(final Note note) {
        notes.removeIf(listNote -> listNote.getId().equals(note.getId()));
        notes.add(note);
    }
}
